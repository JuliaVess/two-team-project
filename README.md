# Two-Team-Project

A website that allows you to monitor the data produced by your RuuviTag sensors. The data to be measured and monitored are temperature and air humidity.

## Technologies

- C#
- JavaScript
- .NET Core
- ASP.NET Core
- Entity Framework Core
- React
- Node.js
- PostgreSQL

## Backend Setup

This guide assumes you are using Visual Studio. This project uses .NET SDK 7.

Open the Backend.sln in Visual Studio. You have to add your database connection string to the User Secrets in the backend project either through right-clicking the project in Visual Studio and selecting "Manage User Secrets", or using the dotnet CLI with `dotnet user-secrets set <key> <value>`. Use the key `"DbConnectionString"`. 

Command Example

```
dotnet user-secrets set "DbConnectionString" "Server=PostgreSQL 15;Host=localhost;Port=5432;Username=postgres;Password=12345;Database=database"
```

You have to do the same as above for the token key by using `"Jwt:Key"`, the key must be at least 16 characters long.

Command Example

```
dotnet user-secrets set "Jwt:Key" "ThisIs16Charssss"
 ```

User Secrets File Example for Both

```json
{
  "DbConnectionString": "Server=PostgreSQL 15;Host=localhost;Port=5432;Username=postgres;Password=12345;Database=database_db",
  "Jwt": {
    "Key":  "SecureKeyThatIsAtLeast16Characters"
  }
}
```

Then you will need to run Update-Database using the Package Manage Console.

### Swagger

When using Swagger, you need to use the User - /api/Users/Login endpoint with valid credentials and enter the token you receive to the field that opens when you press the "Authorize" button in Swagger. Enter the token in format

```
Bearer <token>
```

You can use the User - /api/Users/Register endpoint to create a new user and then use that user to login.

![Authorize button in Swagger](/swagger_authorization.png "Authorize button")

### Backend Setup Without Visual Studio

Run `install_dotnet_etc.bat` to install the needed .NET ans ASP.NET packages through winget. This should need to be done only once.

Run `setup_user_secrets.bat` and configure your Database Connection String and Jwt Key (see above section for more detail). If you need to use spaces in the value, put the value inside quotes.

Run `update_database.bat` to create the tables in the database on the first run, and then run every time after new migrations have been added to keep your database up to date.

Run `start_backend_server.bat` to start the server on your local machine. After starting it up the server will tell where it is listening for requests (by default https://localhost:7154  and http://localhost:5191). To use Swagger, just add `/swagger` after the URL (e.g. https://localhost:7154/swagger).

---

## Frontend Setup

1. To install Frontend packages you need to have `npm` installed from https://nodejs.org/en/download and this repository cloned on your machine.

2. Open terminal and navigate to `frontend` directory. Install dependecies with command `npm install`. This installs the following packages: `react`, `mui`, `axios`, `react-router-dom`.

3. Start frontend with command `npm start` and navigate to localhost:3000 with your browser to run the app.

## API Endpoint Routes

Most of these routes require authorization with the JWT Bearer token. You can obtain the token by using the User login endpoint with valid credentials. You can create a new user through the User register endpoint.

### User

```
- GET     /api/Users/ - Get AllUsers

- GET     /api/Users/{id} - Get user by id

- POST    /api/Users/Register - Register a new user (allowed without token)
          ->  { username: "username", password: "password" }

          optional parameters: { displayname: "displayname",
                                 email: "email",
                                 imageurl: "imageurl"}

- POST    /api/Users/Login - Login and receive token (allowed without token)
          -> { username: "username", password: "password" }
          -> returns token string and user id in response to successful login

- POST    /api/Users/ChangePassword - Change password for currently logged in user
                                      (uses token in backend to check user id)
          -> { oldpassword: "oldpassword", newpassword: "newpassword" }

- PUT     /api/Users/ - Update currently logged in user
                        (can change displayName, email and imageUrl)

- PATCH   /api/Users/ - Patch currently logged in user
                        (can change displayName, email and imageUrl)

- DELETE  /api/Users/ - Delete currently logged in user
```
### Telemetry

```
- GET     /api/Telemetry/GetTelemetrys - Get AllTelemetries

- POST    /api/Telemetry/AddNewTelemetrys - Greate a new telemetry
          ->  { temperature: temperature,
                humidity: humidity,
                airPressure: "airPressure",
                batteryPower: batteryPower,
                movementCounter: movementCounter,
                measureSequence: measureSequence,
                mac: "mac",
                dateTime: "dateTime" }

```
## TODO

- Make the instructions in the Readme how to export the data to Azure IoTHub, decode it there and store it in the Azure PostgreSQL database and from   there to this application.
- Make more endpoints for data processing
