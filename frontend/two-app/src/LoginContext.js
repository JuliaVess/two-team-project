import React, { useEffect, useState, useContext } from "react";
import axios from "axios";
import NotificationContext from "./components/Notification/Notification";
import { useNavigate } from "react-router";

const LoginContext = React.createContext({
  isLoggedIn: false,
  handleLogOut: () => { },
  handleRegister: () => { },
  handleLogIn: () => { },
  getAccessToken: () => { },
  getUserId: () => { },
  getUserName: () => { }
});

export const LoginContextProvider = (props) => {

  const [loggedIn, setLoggedIn] = useState(false);
  const notificationCtx = useContext(NotificationContext);
  const navigate = useNavigate();

  useEffect(() => {
    const alreadyLoggedIn = localStorage.getItem("accessToken");
    if (!!alreadyLoggedIn) {
      setLoggedIn(true);
    }
  }, []);


  const handleLogIn = (username, password) => {
    axios.post("/api/Users/Login", {
      username: username,
      password: password
    }).then((res) => {
      if (res.data.token !== "") {
        localStorage.setItem("accessToken", "Bearer " + res.data.tokenString);
        localStorage.setItem("id", res.data.userId);
        localStorage.setItem("username", username);
        setLoggedIn(true);
        navigate("/houses");
        notificationCtx.success("Welcome!");
      }
    }).catch(() => {
      notificationCtx.error("Ooops... Something is wrong!");
    });
  };


  const handleRegister = (username, password) => {
    axios.post("/api/Users/Register", {
      username: username,
      password: password
    }).then((res) => {
      notificationCtx.success("New user is added");
    }).catch(() => {
      notificationCtx.error("Ooops... Something is wrong!");
    });
  };


  const handleLogOut = () => {
    setLoggedIn(false);
    localStorage.removeItem("accessToken");
    localStorage.removeItem("id");
    notificationCtx.info("User is logged out");
  };


  const getAccessToken = () => {
    return localStorage.getItem("accessToken");
  };


  const getUserId = () => {
    return localStorage.getItem("id");
  };


  const getUserName = () => {
    return localStorage.getItem("username");
  };


  return (
    <LoginContext.Provider
      value={{
        isLoggedIn: loggedIn,
        handleLogOut,
        handleLogIn,
        handleRegister,
        getAccessToken,
        getUserId,
        getUserName
      }}
    >
      {props.children}
    </LoginContext.Provider>
  );
};

export default LoginContext;