import Snackbar from "@mui/material/Snackbar";
import * as React from "react";
import { useState } from "react";
import { Alert } from "@mui/material";


const NotificationContext = React.createContext({
  isDisplayed: false,
  displayMsg: (msg) => { },
  onClose: () => { },
  success: () => { },
  info: () => { },
  error: () => { }
});

const STATES = {
  SUCCESS: "success",
  ERROR: "error",
  INFO: "info"
};

export const NotificationContextProvider = (props) => {

  const [msg, setMsg] = useState("");
  const [notification, setNotification] = useState(null);
  const [isDisplayed, setIsDisplayed] = React.useState(null);


  const success = (msg) => {
    setMsg(msg);
    setIsDisplayed(true);
    setNotification(STATES.SUCCESS);
  };
  const error = (msg) => {
    setIsDisplayed(true);
    setMsg(msg);
    setNotification(STATES.ERROR);
  };
  const info = (msg) => {
    setIsDisplayed(true);
    setMsg(msg);
    setNotification(STATES.INFO);
  };
  const clear = () => {
    setIsDisplayed(null);
    setMsg(null);
    setNotification(null);
  };

  const onClose = (e, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setIsDisplayed(false);
  };


  return (
    <div>
      <NotificationContext.Provider
        value={{
          isDisplayed: false,
          onClose,
          success,
          error,
          info,
          clear,
          msg,
          notification
        }}
      >
        {props.children}
        <Snackbar
          open={isDisplayed}
          autoHideDuration={6000}
          onClose={onClose}
          anchorOrigin={{
            vertical: "top",
            horizontal: "center",
          }}
        >
          <Alert onClose={onClose} severity={notification} sx={{ width: "100%" }}>
            {msg}
          </Alert>
        </Snackbar>
      </NotificationContext.Provider>
    </div>
  );
};

export default NotificationContext;
