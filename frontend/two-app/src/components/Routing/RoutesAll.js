import { Route, Routes } from "react-router-dom";
import LandingPage from "../LandingPage";
import ProtectedRoute from "./ProtectedRoute";
import Test from "../Test";
import HousesList from "../Houses/HousesList";
import RoomsList from "../Rooms/RoomsList";
import AddNewRoom from "../AddRoom/AddNewRoom";
import AccountInfo from "../Account/AccountInfo";


const RoutesAll = () => {
  return (
    <Routes>
      <Route index element={<LandingPage />} />
      <Route path="/test" element={
        <ProtectedRoute>
          <Test />
        </ProtectedRoute>
      }
      />
      <Route path="/houses" element={
        <ProtectedRoute>
          <HousesList />
        </ProtectedRoute>
      }
      />
      <Route path="/houses/remove" element={
        <ProtectedRoute>
          <HousesList />
        </ProtectedRoute>
      }
      />
      <Route path="/rooms/:id" element={
        <ProtectedRoute>
          <RoomsList />
        </ProtectedRoute>
      }
      />
      <Route path="/rooms/:id/remove" element={
        <ProtectedRoute>
          <RoomsList />
        </ProtectedRoute>
      }
      />
      <Route path="/rooms/new" element={
        <ProtectedRoute>
          <AddNewRoom />
        </ProtectedRoute>
      }
      />
      <Route path="/account" element={
        <ProtectedRoute>
          <AccountInfo />
        </ProtectedRoute>
      }
      />
    </Routes>
  );
};

export default RoutesAll;