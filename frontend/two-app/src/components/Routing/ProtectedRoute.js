import { useContext } from "react";
import LoginContext from "../../LoginContext";
import { Navigate } from "react-router";


const ProtectedRoute = (props) => {
  const loginCxt = useContext(LoginContext);

  if (loginCxt.getAccessToken() === null) {
    return <Navigate to="/" replace />;
  }
  return props.children;
};

export default ProtectedRoute;