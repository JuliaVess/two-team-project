import { Button } from "@mui/material";

const GoButton = ({ text, type }) => {

  return (
    <Button
      type={type ? "submit" : null}
      variant="contained"
      sx={{
        marginTop: "20px",
        width: "25%",
        borderRadius: "16px",
        height: "50px",
        backgroundColor: "#0E6163",
        color: "#FCFEFD",
        ":hover": {
          backgroundColor: "#04302b"
        }
      }}>
      {text}
    </Button>
  );
};

export default GoButton;