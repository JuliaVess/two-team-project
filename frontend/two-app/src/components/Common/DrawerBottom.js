import { SwipeableDrawer } from "@mui/material";

const DrawerBottom = ({ open, onClose, onOpen, attachment }) => {

  const drawerStyle = {
    sx: {
      height: "60%",
      width: "90%",
      marginLeft: "5%",
      borderRadius: "16px",
      alignItems: "center",
      fontSize: "50px",
      backgroundColor: "#D9D9D9",
      opacity: "85%"
    }
  };

  return (
    <SwipeableDrawer
      anchor="bottom"
      open={open}
      onClose={onClose}
      onOpen={onOpen}
      PaperProps={drawerStyle}
    >
      <>{attachment}</>
    </SwipeableDrawer>
  );
};

export default DrawerBottom;