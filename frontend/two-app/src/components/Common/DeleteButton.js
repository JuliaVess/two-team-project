import DeleteForeverIcon from "@mui/icons-material/DeleteForever";

const DeleteButton = ({ onClick }) => {

  return (
    <DeleteForeverIcon
      onClick={onClick}
      sx={{
        position: "absolute",
        height: "40px",
        width: "40px",
        marginTop: "20px",
        marginLeft: "40%"
      }} />
  );
};

export default DeleteButton;