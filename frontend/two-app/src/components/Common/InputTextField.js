import { TextField } from "@mui/material";

const InputTextField = ({ name, type }) => {
  // textTransform : "uppercase"

  return (
    <TextField
      type={type}
      label={name}
      name={name}
      inputProps={{
        maxLength: 32,
        minLength: 6,
        style: { color: "#FCFEFD" }
      }}
      InputLabelProps={{
        style: { color: "#0E6163" }
      }}
      sx={{
        width: "40%",
        marginTop: "20px",
        "& fieldset": {
          borderRadius: "16px",
          borderColor: "#0E6163"
        },
        "& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline": {
          borderColor: "#0E6163"
        },
      }}
      required
    />
  );
};

export default InputTextField;