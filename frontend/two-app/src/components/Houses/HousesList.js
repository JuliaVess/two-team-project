import OneHouseCard from "./OneHouseCard";
import houses from "../../Houses";


const HousesList = () => {


  return (
    <>
      {houses && houses.map(h =>
        <OneHouseCard house={h.houseName} key={h.id}
          index={houses.indexOf(h)} />
      )}
    </>
  );
};

export default HousesList;