import { Card, CardActionArea, CardContent, Typography } from "@mui/material";
import { useContext, useState } from "react";
import { useLocation, useNavigate } from "react-router";
import AlertWindow from "../Alert/AlertWindow";
import DeleteButton from "../Common/DeleteButton";
import NotificationContext from "../Notification/Notification";
import houses from "../../Houses";


const OneHouse = ({ house, index }) => {

  const navigate = useNavigate();
  const location = useLocation();
  const notificationCtx = useContext(NotificationContext);

  const [openAlert, setOpenAlert] = useState(false);

  const cartStyle = {
    position: "relative",
    backgroundColor: "#1E5559",
    color: "#FFFFFF",
    marginTop: "20px",
    borderRadius: "16px",
    textAlight: "center"
  };

  const textStyle = {
    position: "absolute",
    width: "80%",
    textTransform: "uppercase",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)"
  };

  const squareStyle = {
    width: "60%",
    backgroundColor: "#D9D9D9",
    height: "220px",
    borderRadius: "16px",
    opacity: "10%",
    position: "relative",
    marginLeft: "20%"
  };


  const handleCart = (e) => {
    e.preventDefault();
    localStorage.setItem("index", index);

    if (location.pathname === "/houses/remove") {
      setOpenAlert(true);
    } else {
      navigate(`/rooms/${index}`);
    }
  };

  const handleRemove = () => {
    houses.splice(index, 1); //@TODO replace to axios

    setOpenAlert(false);
    notificationCtx.success("place is removing");
    navigate("/houses");
  };

  const handleClose = () => {
    navigate(-1);
    setOpenAlert(false);
  };

  return (
    <>
      <Card variant="elevation" elevation={12} sx={cartStyle} >
        {location.pathname === "/houses/remove" && <DeleteButton />}
        <CardActionArea onClick={handleCart}>
          <CardContent>
            <Typography fontSize={45} fontWeight={"bold"} sx={textStyle}>
              {house}
            </Typography>
            <Typography sx={squareStyle} />
          </CardContent>
        </CardActionArea>
      </Card>

      <AlertWindow open={openAlert} text="place"
        onClose={handleClose} handleRemove={handleRemove} />
    </>
  );
};

export default OneHouse;