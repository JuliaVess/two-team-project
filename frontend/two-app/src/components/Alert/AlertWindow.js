import {
  Dialog, DialogActions, DialogContent,
  DialogContentText,
  Slide
} from "@mui/material";
import { Button } from "@mui/material";
import React from "react";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const AlertWindow = ({ open, onClose, handleRemove, text }) => {

  return (
    <>
      <Dialog
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={onClose}
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">
            Are you sure you want to delete your {text}?
          </DialogContentText>
          <DialogActions>
            <Button onClick={onClose}>No</Button>
            <Button onClick={handleRemove}>Yes</Button>
          </DialogActions>
        </DialogContent>
      </Dialog>
    </>
  );
};

export default AlertWindow;