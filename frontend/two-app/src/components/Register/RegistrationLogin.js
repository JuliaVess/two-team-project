import { Grid, Button } from "@mui/material";
import GoButton from "../Common/GoButton";
import InputTextField from "../Common/InputTextField";
import { useContext, useState } from "react";
import LoginContext from "../../LoginContext";


const RegistrationLogin = () => {

  const [register, setRegister] = useState(false);
  const loginCtx = useContext(LoginContext);


  const handleButton = (e) => {
    e.preventDefault();

    register ? setRegister(false) : setRegister(true);
  };


  const handleButtonRegisterLogin = async e => {
    e.preventDefault();


    const username = e.target.username.value;
    const password = e.target.password.value;


    if (register) {
      loginCtx.handleRegister(username, password);
      setRegister(false);
    } else {
      loginCtx.handleLogIn(username, password);
    }

  };


  return (

    <form onSubmit={handleButtonRegisterLogin}>

      <Grid
        container
        direction="column"
        justifyContent="center"
        alignItems="center"
        sx={{ marginTop: "50px" }}>

        <InputTextField name="username" type="text" />
        <InputTextField name="password" type="password" />

        <GoButton text={register ? "register" : "go"} type="submit" />
        <Button onClick={handleButton}
          sx={{
            color: "#398B81",
            fontFamily: "Segoe UI",
            fontSize: "10px"
          }}
          size="small">
          {register ? "login" : "New User Registration"}
        </Button>

      </Grid>

    </form>
  );

};

export default RegistrationLogin;