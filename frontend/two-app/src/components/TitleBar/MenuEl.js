import { Menu, MenuItem } from "@mui/material";
import { useContext } from "react";
import { useLocation, useNavigate } from "react-router";
import LoginContext from "../../LoginContext";

const MenuEl = ({ anchorEl, setAnchorEl }) => {

  const loginCtx = useContext(LoginContext);
  const navigate = useNavigate();
  const location = useLocation();


  const handleLogOut = () => {
    loginCtx.handleLogOut();
    navigate("/houses");
    setAnchorEl(null);
  };

  const handleRemove = () => {

    if (location.pathname === "/houses") {
      navigate("/houses/remove");
    } else {
      const id = localStorage.getItem("index");
      navigate(`/rooms/${id}/remove`);
    };

  };

  return (
    <Menu
      id="menu-appbar"
      anchorEl={anchorEl}
      anchorOrigin={{
        vertical: "top",
        horizontal: "right"
      }}
      keepMounted
      transformOrigin={{
        vertical: "top",
        horizontal: "right"
      }}
      PaperProps={{
        sx: {
          width: "40%",
          borderRadius: "10px",
          backgroundColor: "#D9D9D9",
          opacity: "10%",
          color: "#001D1B"
        }
      }}
      open={Boolean(anchorEl)}
      onClose={() => setAnchorEl(null)}
    >
      <MenuItem onClick={() => navigate("/rooms/new")}>
        Add new room
      </MenuItem>

      <MenuItem onClick={handleRemove}>
        {location.pathname === "/houses" ? "Remove house" : "Remove room"}
      </MenuItem>

      <MenuItem onClick={handleLogOut}
        sx={{ paddingTop: "30px", float: "right", color: "red" }}>
        Log out
      </MenuItem>

      <MenuItem onClick={() => navigate("/account")}
        sx={{ paddingTop: "30px", float: "right", color: "green" }}>
        Account
      </MenuItem>

    </Menu>
  );
};

export default MenuEl;