import { AppBar, IconButton, Toolbar, Box, Grid } from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";
import KeyboardArrowLeftIcon from "@mui/icons-material/KeyboardArrowLeft";
import { useContext, useState } from "react";
import LoginContext from "../../LoginContext";
import { useLocation, useNavigate } from "react-router";
import MenuEl from "./MenuEl";


const TitleBar = () => {
  const [anchorEl, setAnchorEl] = useState(false);
  const loginCtx = useContext(LoginContext);
  const navigate = useNavigate();
  const location = useLocation();

  const sizeStyle = {
    height: "70px",
    width: "70px"
  };

  const handleBack = () => {
    navigate("/houses");
    setAnchorEl(null);
  };

  if (loginCtx.getAccessToken() !== null) {
    return (
      <Box sx={{ flexGrow: 1 }}>
        <AppBar position="static"
          sx={{
            borderRadius: "10px",
            backgroundColor: "#001D1B"
          }}>
          <Toolbar>

            <Grid container justifyContent="space-between">
              <IconButton
                size="large"
                edge="start"
                aria-label="back button"
                color="inherit"
                onClick={handleBack}
                sx={{
                  alignContent: "left"
                }}
                disabled={location.pathname !== "/houses" ?
                  false : true}
              >
                <KeyboardArrowLeftIcon sx={sizeStyle} />
              </IconButton>

              <IconButton
                size="large"
                edge="end"
                color="inherit"
                aria-label="menu"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                onClick={(e) => setAnchorEl(e.currentTarget)}
              >
                <MenuIcon sx={sizeStyle} />
              </IconButton>
            </Grid>

            <MenuEl anchorEl={anchorEl} setAnchorEl={setAnchorEl} />

          </Toolbar>
        </AppBar>
      </Box >

    );
  }
  return null;
};

export default TitleBar;