import { Typography, Box, Grid, Button } from "@mui/material";
import UserAvatar from "./UserAvatar";
import AccountInfoText from "./AccountInfoText";
import AlertWindow from "../Alert/AlertWindow";
import { useState, useContext } from "react";
import LoginContext from "../../LoginContext";
import NotificationContext from "../Notification/Notification";
import axios from "axios";
import { useEffect } from "react";
import AccuantPassword from "./AccountPassword";


const AccountInfo = () => {

  const [openAlert, setOpenAlert] = useState(false);
  const [editInfo, setEditInfo] = useState(false);
  const [editPswrd, setEditPswrd] = useState(false);

  const loginCtx = useContext(LoginContext);
  const notificationCtx = useContext(NotificationContext);

  const [userData, setUserData] = useState([]);
  const id = loginCtx.getUserId();


  useEffect(() => {
    axios.get(`https://localhost:7154/api/Users/${id}`)
      .then(res => {
        setUserData(res.data);
      })
      .catch(() => {
        notificationCtx.error("Can't get the User data");
      });
  }, []);


  const DeleteAccount = () => {

    setOpenAlert(false);
    loginCtx.handleLogOut();
    notificationCtx.success("User is removing");

    //@TODO: delete accuant from dataBase
  };

  const handleDeleteSave = (e) => {
    e.preventDefault();

    if (!editInfo && !editPswrd) {
      setOpenAlert(true);

    } else if (editPswrd) {

      const changePswrd = {
        oldPassword: e.target.oldPswrd.value,
        newPassword: e.target.newPswrd.value
      };

      axios.post("https://localhost:7154/api/Users/ChangePassword", changePswrd)
        .then(() => {
          notificationCtx.success("Password changed");
        })
        .catch(() => {
          notificationCtx.error("Can't change the password");
        });

      setEditPswrd(false);

    } else {

      const newDisplayName =
      {
        operationType: 0,
        path: "/DisplayName",
        op: "replace",
        from: "string",
        value: e.target.displayName.value
      };

      const newEmail =
      {
        operationType: 0,
        path: "/Email",
        op: "replace",
        from: "string",
        value: e.target.email.value
      };

      axios.patch("https://localhost:7154/api/Users", newDisplayName)
        .then((res) => {
          axios.patch("https://localhost:7154/api/Users", newEmail)
            .catch((err) => {
              notificationCtx.error("Ooops.., something is wrong");
            });
          notificationCtx.success("Data has been changed");
        })
        .catch((err) => {
          notificationCtx.error("Ooops.., something is wrong");
        });

      setEditInfo(false);

    }
  };


  return (
    <>
      <UserAvatar />

      <Typography
        variant="h4"
        sx={{ fontWeight: "bold" }}>
        Account Information
      </Typography>

      <form onSubmit={handleDeleteSave}>
        <Box
          sx={{ padding: "10px", marginLeft: "30%", paddingTop: "50px" }}>

          {editPswrd ?
            <>
              <AccuantPassword oldName="oldPswrd" newName="newPswrd" />
            </>
            :
            <>
              <AccountInfoText label="Display Name" name="displayName"
                data={userData.displayName} editInfo={editInfo} />
              <AccountInfoText label="User Name" name="userName"
                data={userData.userName} editInfo={editInfo} />
              <AccountInfoText label="Email" name="email"
                data={userData.email} editInfo={editInfo} />
            </>
          }
        </Box>

        <Grid sx={{ paddingTop: "50px" }}>

          {!editPswrd &&
            <Button variant="conteiner" onClick={() => setEditInfo(editInfo => !editInfo)}
              sx={{
                fontSize: "20px"
              }}>
              {editInfo ? "Back" : "Edit info"}
            </Button>
          }

          {!editInfo &&
            <Button variant="conteiner" onClick={() => setEditPswrd(editPswrd => !editPswrd)}
              sx={{
                fontSize: "20px"
              }}>
              {editPswrd ? "Back" : "Edit password"}
            </Button>
          }

        </Grid >

        <Button type="submit"
          sx={{
            fontSize: "20px"
          }}>
          {editInfo || editPswrd ? "Save changes" : "Delete accuant"}
        </Button>

      </form>


      <AlertWindow open={openAlert} text="accuant"
        onClose={() => setOpenAlert(false)} handleRemove={DeleteAccount} />
    </>
  );
};

export default AccountInfo;