import { TextField } from "@mui/material";


const AccountInfoInput = ({ data, name }) => {


  return (
    <TextField
      size="small"
      defaultValue={data}
      name={name}
      sx={{
        "& fieldset": {
          borderRadius: "16px",
        },
        "& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline": {
          borderColor: "#0E6163"
        },
      }}
      inputProps={{
        maxLength: 32,
        minLength: 6,
        style: { color: "#FCFEFD" }
      }}
      InputLabelProps={{
        style: { color: "#0E6163" }
      }} />
  );
};

export default AccountInfoInput;