import { Grid, TextField } from "@mui/material";


const AccuantPassword = ({ oldName, newName }) => {

  const style = {
    width: "80%",
    marginTop: "20px",
    "& fieldset": {
      borderRadius: "16px",
      borderColor: "#0E6163"
    },
    "& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline": {
      borderColor: "#0E6163"
    },
  };

  return (
    <Grid container
      direction="column"
      justifyContent="center"
      alignItems="center"
      sx={{
        marginTop: "10px",
        marginLeft: "-100px"
      }}>

      <TextField
        label="Enter your old password"
        name={oldName}
        inputProps={{
          maxLength: 32,
          minLength: 6,
          style: { color: "#FCFEFD" }
        }}
        InputLabelProps={{
          style: { color: "#0E6163" }
        }}
        sx={style}
        required
      />

      <TextField
        label="Enter your new password"
        name={newName}
        inputProps={{
          maxLength: 32,
          minLength: 6,
          style: { color: "#FCFEFD" }
        }}
        InputLabelProps={{
          style: { color: "#0E6163" }
        }}
        sx={style}
        required
      />

    </Grid>
  );
};

export default AccuantPassword;