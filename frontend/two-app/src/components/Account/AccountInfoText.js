import { Grid, Typography } from "@mui/material";
import AccountInfoInput from "./AccountInfoInput";


const AccountInfoText = ({ label, data, editInfo, name }) => {

  return (
    <Grid container
      direction="row"
      spacing={6}
      sx={{
        padding: "5px"
      }}
    >
      <Grid item xs="auto">
        <Typography fontWeight="bold" fontSize={24} sx={{ paddingBottom: "20px" }}>
          {label}
        </Typography>
      </Grid>
      <Grid item xs="auto" >

        {editInfo ?
          <AccountInfoInput name={name} data={data} />
          :
          <Typography variant="h5"> {data} </Typography>
        }

      </Grid>
    </Grid>
  );
};

export default AccountInfoText;