import { Avatar } from "@mui/material";
import Box from "@mui/material/Box";
import { useContext } from "react";
import LoginContext from "../../LoginContext";


const UserAvatar = () => {

  const loginCtx = useContext(LoginContext);
  const userName = loginCtx.getUserName();


  return (
    <><Box sx={{
      marginLeft: "33%",
      marginTop: "150px",
      marginBottom: "50px"
    }}>
      <Avatar sx={{
        bgcolor: "#00695c",
        width: "260px",
        height: "260px",
        fontSize: "40px"
      }}>
        {userName}
      </Avatar>
    </Box>
    </>
  );
};

export default UserAvatar;