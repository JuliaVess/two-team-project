import { Box, Card, CardActionArea, Grid } from "@mui/material";
import { CardContent, Collapse, Typography } from "@mui/material";
import { useContext, useState } from "react";
import DrawerBottom from "../Common/DrawerBottom";
import BarChartIcon from "@mui/icons-material/BarChart";
import SettingsIcon from "@mui/icons-material/Settings";
import { useLocation, useNavigate } from "react-router";
import DeleteButton from "../Common/DeleteButton";
import AlertWindow from "../Alert/AlertWindow";
import NotificationContext from "../Notification/Notification";
import houses from "../../Houses";


const OneRoomCard = ({ rooms, houseIndex, indexRoom }) => {

  const [open, setOpen] = useState(false);
  const [openSettigs, setOpenSettings] = useState(false);
  const [openHistoria, setOpenHistoria] = useState(false);
  const [openAlert, setOpenAlert] = useState(false);

  const location = useLocation();
  const navigate = useNavigate();
  const notificationCtx = useContext(NotificationContext);


  const cartStyle = {
    position: "relative",
    backgroundColor: "#1E5559",
    color: "#FFFFFF",
    marginTop: "20px",
    borderRadius: "16px",
    textAlight: "center"
  };

  const textStyle = {
    padding: "5%",
    margin: "45px auto",
    position: "absolute",
    width: "40%",
    textTransform: "uppercase"
  };

  const squareStyle = {
    width: "50%",
    backgroundColor: "#D9D9D9",
    height: "200px",
    borderRadius: "16px",
    opacity: "10%",
    position: "relative",
    margin: "20px"
  };

  const boxStyle = {
    color: "white",
    position: "absolute",
    marginLeft: "65%",
    marginTop: "-30%",
    backgroundColor: "rgb(0, 0, 0, 0)"
  };


  const handleCart = (e) => {
    e.preventDefault();

    if (location.pathname === `/rooms/${houseIndex}/remove`) {
      setOpenAlert(true);
      setOpen(false);
    }
    else {
      setOpen(!open);
    }
  };

  const handleRemove = () => {

    houses[houseIndex].rooms.splice(indexRoom, 1);

    setOpenAlert(false);
    notificationCtx.success("room is removing");

    navigate(-1);
  };

  const handleClose = () => {
    navigate(-1);
    setOpenAlert(false);
  };


  return (
    <>
      <Card variant="elevation" elevation={12} sx={cartStyle} >

        {location.pathname === `/rooms/${houseIndex}/remove` && <DeleteButton />}

        <CardActionArea onClick={handleCart}>
          <CardContent>

            <Typography fontSize={50} fontWeight={"bold"} sx={textStyle} >
              {rooms.roomName}
            </Typography>
            <Typography sx={squareStyle} />

            <Box sx={boxStyle}>
              <Typography fontSize={80} fontWeight={"bold"}>
                {rooms.lampotila}
              </Typography>
              <Typography fontSize={50} >
                {rooms.koosteus}
              </Typography>
            </Box>

          </CardContent>
        </CardActionArea>

        <Collapse in={open} timeout="auto" unmountOnExit>
          <Grid container justifyContent="space-around">
            <Grid item xs={5} >
              <SettingsIcon onClick={() => setOpenSettings(true)}
                sx={{
                  width: "60px",
                  height: "60px",
                  float: "left"
                }} />
            </Grid>
            <Grid item xs={5} >
              <BarChartIcon onClick={() => setOpenHistoria(true)}
                sx={{
                  width: "60px",
                  height: "60px",
                  float: "right",
                  marginBottom: "15px"
                }} />
            </Grid>
          </Grid>
        </Collapse>

      </Card>

      <DrawerBottom open={openSettigs} onOpen={() => setOpenSettings(true)}
        onClose={() => setOpenSettings(false)} attachment="settings" />

      <DrawerBottom open={openHistoria} onOpen={() => setOpenHistoria(true)}
        onClose={() => setOpenHistoria(false)} attachment="historia" />

      <AlertWindow open={openAlert} text="room"
        onClose={handleClose} handleRemove={handleRemove} />
    </>
  );
};

export default OneRoomCard;