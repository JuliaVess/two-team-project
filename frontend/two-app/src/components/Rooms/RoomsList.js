import OneRoomCard from "./OneRoomCard";
import houses from "../../Houses";
import { useParams } from "react-router";


const RoomsList = () => {

  const { id } = useParams();


  return (
    <>
      {houses[id].rooms && houses[id].rooms.map(h =>
        <OneRoomCard rooms={h} key={h.id} indexRoom={houses[id].rooms.indexOf(h)}
          houseIndex={id} />
      )}
    </>
  );
};

export default RoomsList;