import { TextField } from "@mui/material";

const TextEnterField = ({ name, label }) => {

  return (
    <TextField
      type="text"
      label={label}
      name={name}
      inputProps={{
        maxLength: 32,
        minLength: 4,
        style: {
          color: "#FCFEFD",
          textTransform: "uppercase"
        }
      }}
      InputLabelProps={{
        style: { color: "#0E6163" }
      }}
      sx={{
        width: "60%",
        marginTop: "20px",
        "& fieldset": {
          borderRadius: "16px",
          borderColor: "#0E6163"
        },
        "& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline": {
          borderColor: "#0E6163"
        },
      }}
      required
    />
  );
};

export default TextEnterField;