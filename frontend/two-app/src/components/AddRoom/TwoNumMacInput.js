import { TextField, Typography } from "@mui/material";


const TwoMunMacInput = ({ name, setMacNum, macNum }) => {


  const inputStyle = {
    width: "50px",
    margin: "10px",
    "& fieldset": {
      borderRadius: "10px",
      borderColor: "#0E6163"
    },
    "& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline": {
      borderColor: "#0E6163"
    }
  };

  const inputPropsStyle = {
    maxLength: 2,
    minLength: 2,
    style: {
      color: "#FCFEFD",
      textTransform: "uppercase",
      textAlign: "center"
    }
  };


  const handleChange = (e) => {

    const { maxLength, value, name } = e.target;
    const [fieldName, fieldIndex] = name.split("-");


    if (value.length >= maxLength) {
      if (parseInt(fieldIndex, 10) < 6) {
        const nextSibling = document.querySelector(
          `input[name=ssn-${parseInt(fieldIndex, 10) + 1}]`
        );

        setMacNum([
          ...macNum,
          value
        ]);

        if (nextSibling !== null) {
          nextSibling.focus();
        }
      }
    }

    //rendering for the last two numbers
    if (macNum.length >= 5 && value.length >= 2) {
      setMacNum([
        ...macNum,
        value
      ]);
    }

  };


  return (
    <>
      <TextField
        name={name}
        onChange={handleChange}
        inputProps={inputPropsStyle}
        InputLabelProps={{
          style: { color: "#0E6163" }
        }}
        sx={inputStyle}
        required />

      {name !== "ssn-6" ? <Typography>:</Typography> : null}
    </>
  );
};

export default TwoMunMacInput;