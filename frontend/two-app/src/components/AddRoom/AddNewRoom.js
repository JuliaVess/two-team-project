import { TextField, MenuItem, Grid, Divider } from "@mui/material";
import { useContext, useState } from "react";
import { useNavigate } from "react-router";
import GoButton from "../Common/GoButton";
import TextMacNum from "./TextMacNum";
import NotificationContext from "../Notification/Notification";
import TextEnterField from "./TextEnterField";
import houses from "../../Houses";


const AddNewRoom = () => {

  const notificationCtx = useContext(NotificationContext);
  const navigate = useNavigate();

  const [newPlace, setNewPlace] = useState();
  const [macNum, setMacNum] = useState([]);


  const handleSave = (e) => {
    e.preventDefault();

    const macNumber = macNum.join(":");
    const placeName = (newPlace === "addNew" ? e.target.placeName.value : newPlace);


    if (newPlace !== "addNew") {
      const index = houses.findIndex(el => el.houseName === placeName);

      const room = {
        id: macNumber,
        roomName: e.target.roomName.value,
        lampotila: "24°C",
        koosteus: "18%"
      };

      houses[index].rooms.push(room);

    } else {
      const newRoom = {
        id: 678,
        houseName: placeName,
        rooms: [{
          id: macNumber,
          roomName: e.target.roomName.value,
          lampotila: "22°C",
          koosteus: "24%"
        }]
      };

      houses.push(newRoom);
    }


    navigate("/houses");
    notificationCtx.success("new place is added");
  };

  return (
    <>
      <form onSubmit={handleSave} style={{ marginTop: "30%" }}>

        <Grid
          container
          direction="column"
          justifyContent="center"
          alignItems="center">


          <TextField
            value={newPlace || ""}
            onChange={(e) => setNewPlace(e.target.value)}
            select
            name="placeList"
            label="Choose the place"
            inputProps={{
              maxLength: 32,
              minLength: 4
            }}
            InputLabelProps={{
              style: { color: "#0E6163" }
            }}
            sx={{
              width: "60%",
              marginTop: "20px",
              "& fieldset": {
                borderRadius: "16px",
                borderColor: "#0E6163"
              },
              "& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline": {
                borderColor: "#0E6163"
              },
              "& .MuiInputBase-root": {
                color: "#FFFFFF",
                textTransform: "uppercase"
              }
            }}
            required
          >

            {houses && houses.map(h =>
              <MenuItem
                sx={{
                  backgroundColor: "#D9D9D9",
                  opacity: "85%",
                  fontSize: "20px",
                  textTransform: "uppercase"
                }}
                key={h.id}
                value={h.houseName}
              >{h.houseName}</MenuItem>
            )}

            <MenuItem value="addNew" sx={{ color: "red" }}>
              Add new place...
            </MenuItem>

          </TextField>


          {newPlace === "addNew" &&
            <TextEnterField name="placeName" label="Give the name of the place" />
          }

          <TextEnterField name="roomName" label="Enter roon name" />

          <Divider
            sx={{
              marginTop: "20px",
              "&.MuiDivider-root": {
                "&::before, &::after": {
                  borderTop: "red"
                },
              }
            }}
          >
            MAC NUMBER
          </Divider>


          <Grid container
            direction="row"
            justifyContent="center"
            alignItems="center"
          >
            <TextMacNum macNum={macNum} setMacNum={setMacNum} />

          </Grid>

          <GoButton text="save" type="submit" />

        </Grid>
      </form>
    </>
  );
};

export default AddNewRoom;