import TwoNunMacInput from "./TwoNumMacInput";


const TextMacNum = ({ macNum, setMacNum }) => {
  const textFields = [];
  for (let i = 1; i <= 6; i++) {
    textFields.push(
      <TwoNunMacInput key={i} name={`ssn-${i}`} macNum={macNum} setMacNum={setMacNum} />);
  }


  return (
    <>
      {textFields}
    </>
  );
};

export default TextMacNum;