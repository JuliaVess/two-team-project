import { Typography } from "@mui/material";
import RegistrationLogin from "./Register/RegistrationLogin";


const LandingPage = () => {

  return (
    <>
      <Typography
        sx={{ fontSize: "80px", fontFamily: "Segoe UI", paddingTop: "30%" }} >
        Welcome To
        <br />
        Two-App
      </Typography>

      <RegistrationLogin />
    </>
  );
};

export default LandingPage;