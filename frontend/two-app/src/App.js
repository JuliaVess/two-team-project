import { Container } from "@mui/material";
import { BrowserRouter } from "react-router-dom";
import { NotificationContextProvider } from "./components/Notification/Notification";
import RoutesAll from "./components/Routing/RoutesAll";
import TitleBar from "./components/TitleBar/TitleBar";
import { LoginContextProvider } from "./LoginContext";


function App() {

  const style = {
    backgroundColor: "#001D1B",
    color: "#419A8F",
    height: "1150px",
    textAlign: "center"
  };

  return (
    <Container style={style}>
      <BrowserRouter>
        <NotificationContextProvider>
          <LoginContextProvider>
            <TitleBar />
            <RoutesAll />
          </LoginContextProvider>
        </NotificationContextProvider>
      </BrowserRouter>
    </Container>
  );
};

export default App;