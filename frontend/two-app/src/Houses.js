
const houses = [
  {
    id: 1,
    houseName: "Mökki",
    rooms: [{
      id: 10,
      roomName: "olohuone",
      lampotila: "20°C",
      koosteus: "23%"
    },
    {
      id: 20,
      roomName: "sauna",
      lampotila: "38°C",
      koosteus: "19%"
    }
    ]
  },
  {
    id: 2,
    houseName: "Omakotitalo Helsinki",
    rooms: [{
      id: 10,
      roomName: "eteinen",
      lampotila: "23°C",
      koosteus: "23%"
    },
    {
      id: 20,
      roomName: "makuhuone",
      lampotila: "18°C",
      koosteus: "11%"
    }
    ]
  },
  {
    id: 3,
    houseName: "Kerrostalo Tampere",
    rooms: [{
      id: 10,
      roomName: "vessä",
      lampotila: "22°C",
      koosteus: "35%"
    },
    {
      id: 20,
      roomName: "sauna",
      lampotila: "49°C",
      koosteus: "11%"
    }
    ]
  }
];

export default houses;