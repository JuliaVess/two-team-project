@echo off
cd Backend\TwoTeamProject
setlocal
echo Your currently set user-secrets are:
echo -------------------------------
dotnet user-secrets list
echo -------------------------------
set /p cstring=Please enter your Database Connection String: 
dotnet user-secrets set "DbConnectionString" %cstring%
set /p jwtkey=Please enter your Jwt Key: 
dotnet user-secrets set "Jwt:Key" %jwtkey%
pause