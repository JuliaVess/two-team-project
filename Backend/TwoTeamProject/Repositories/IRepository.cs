﻿using TwoTeamProject.Models;

namespace TwoTeamProject.Repositories
{
    public interface IRepository<T> where T : class, IEntity
    {
        public List<T> GetAll();
        public T GetById(int id);
        public T Add(T entity);
        public void Update(int id, T entity);
        public void Delete(int id);
    }
}
