﻿using TwoTeamProject.Models.User;

namespace TwoTeamProject.Repositories
{
    public interface IUserRepository : IRepository<User>
    {
        public User GetByEmail(string email);
        public User GetByUsername(string userName);
    }
}
