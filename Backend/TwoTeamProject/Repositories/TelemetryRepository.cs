﻿using System;
using Microsoft.EntityFrameworkCore;
using TwoTeamProject.Models.Telemetry;

namespace TwoTeamProject.Repositories
{
    public class TelemetryRepository : RepoBase<Telemetry>, ITelemetryRepository
    {
        public TelemetryRepository(TwoTeamProjectContext context) : base(context) { }
        
        public List<Telemetry> GetAllTelemetries()
        {
            return _context.Set<Telemetry>().ToList();
        }
    }
}
