﻿using TwoTeamProject.Models;

namespace TwoTeamProject.Repositories
{
    public abstract class RepoBase<T> : IRepository<T> where T : class, IEntity
    {
        protected readonly TwoTeamProjectContext _context;

        public RepoBase(TwoTeamProjectContext context)
        {
            _context = context;
        }

        public virtual List<T> GetAll()
        {
            return _context.Set<T>().ToList();
        }

        public virtual T GetById(int id)
        {
            return _context.Set<T>().FirstOrDefault(entity => entity.Id == id);
        }

        public virtual T Add(T entity)
        {
            entity.Id = 0;
            _context.Set<T>().Add(entity);
            _context.SaveChanges();
            return entity;
        }

        public virtual void Update(int id, T entity)
        {
            var baseEntity = GetById(id);
            UpdateEntity(ref baseEntity, entity);
            _context.Update(baseEntity);
            _context.SaveChanges();
        }

        public virtual void Delete(int id)
        {
            _context.Remove(GetById(id));
            _context.SaveChanges();
        }

        public virtual void UpdateEntity(ref T baseEntity, T modifiedEntity)
        {
            Type type = baseEntity.GetType();

            foreach (var property in type.GetProperties())
            {
                if (property.Name != "Id")
                {
                    property.SetValue(baseEntity, property.GetValue(modifiedEntity));
                }
            }
        }
    }
}
