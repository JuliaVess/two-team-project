﻿using Microsoft.EntityFrameworkCore;
using TwoTeamProject.Models.Telemetry;
using TwoTeamProject.Models.User;

namespace TwoTeamProject.Repositories
{
    public class TwoTeamProjectContext : DbContext
    {
        static TwoTeamProjectContext()
        {
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
        }
        public DbSet<User> Users { get; set; } = null!;
        public DbSet<Telemetry> Telemetries { get; set; } = null!;

        public TwoTeamProjectContext(DbContextOptions<TwoTeamProjectContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasAlternateKey(r => r.UserName);
        }
    }
}
