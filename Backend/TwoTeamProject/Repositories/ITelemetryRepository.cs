﻿using TwoTeamProject.Models.Telemetry;

namespace TwoTeamProject.Repositories
{
    public interface ITelemetryRepository : IRepository<Telemetry>
    {
        public List<Telemetry> GetAllTelemetries();
    }
}
