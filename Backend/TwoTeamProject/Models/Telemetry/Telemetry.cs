﻿namespace TwoTeamProject.Models.Telemetry
{
    public class Telemetry : IEntity
    {
        public int Id { get; set; }
        public double Temperature { get; set; }
        public double Humidity { get; set; }
        public string AirPressure { get; set; }
        public double BatteryPower { get; set; }
        public double MovementCounter { get; set; }
        public double MeasureSequence { get; set; }
        public string Mac { get; set; }
        public DateTime DateTime { get; set; } = DateTime.Now;
    }
}
