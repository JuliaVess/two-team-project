﻿namespace TwoTeamProject.Models
{
    public interface IEntity
    {
        public int Id { get; set; }
    }
}
