﻿using System.ComponentModel.DataAnnotations;

namespace TwoTeamProject.Models.User
{
    public class PasswordChangeData
    {
        [Required]
        public string OldPassword { get; set; }
        [Required]
        public string NewPassword { get; set; }
    }
}
