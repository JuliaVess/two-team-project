﻿using System.ComponentModel.DataAnnotations;

namespace TwoTeamProject.Models.User
{
    public class User : IEntity
    {
        public int Id { get; set; }

        [Required]
        public string UserName { get; set; }

        [Required]
        public string PasswordHash { get; set; }

        public string? DisplayName { get; set; }

        [EmailAddress(ErrorMessage = "Invalid email address.")]
        public string? Email { get; set; }
        public string? ImageUrl { get; set; }
    }
}
