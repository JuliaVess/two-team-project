﻿using System.ComponentModel.DataAnnotations;

namespace TwoTeamProject.Models.User
{
    public class UserData
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        public string Password { get; set; }

        public string? DisplayName { get; set; }

        [EmailAddress(ErrorMessage = "Invalid email address.")]
        public string? Email { get; set; }
        public string? ImageUrl { get; set; }
    }
}
