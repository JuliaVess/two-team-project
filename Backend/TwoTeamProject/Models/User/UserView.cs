﻿using System.ComponentModel.DataAnnotations;

namespace TwoTeamProject.Models.User
{
    public class UserView
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string? DisplayName { get; set; }

        [EmailAddress(ErrorMessage = "Invalid email address.")]
        public string? Email { get; set; }
        public string? ImageUrl { get; set; }

        public UserView(User user)
        {
            Id = user.Id;
            UserName = user.UserName;
            DisplayName = user.DisplayName;
            Email = user.Email;
            ImageUrl = user.ImageUrl;
        }

        public UserView()
        {
        }

        public static explicit operator UserView(User user) => new UserView(user);
    }
}
