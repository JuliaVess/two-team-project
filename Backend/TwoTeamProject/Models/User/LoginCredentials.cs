﻿using System.ComponentModel.DataAnnotations;

namespace TwoTeamProject.Models.User
{
    public struct LoginCredentials
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
