﻿using Microsoft.IdentityModel.Tokens;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using TwoTeamProject.Models.User;
using TwoTeamProject.Repositories;

namespace TwoTeamProject.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _repository;
        private readonly IConfiguration _configuration;

        public UserService(IUserRepository repository, IConfiguration configuration)
        {
            _repository = repository;
            _configuration = configuration;
        }

        public User GetById(int id) =>
            _repository.GetById(id);

        public List<User> GetAll() =>
            _repository.GetAll();

        public void Update(int id, UserUpdate userUpdate)
        {
            var modifiedUser = GetById(id);
            modifiedUser.DisplayName = userUpdate.DisplayName;
            modifiedUser.Email = userUpdate.Email;
            modifiedUser.ImageUrl = userUpdate.ImageUrl;
            _repository.Update(id, modifiedUser);
        }

        public User? Delete(int id)
        {
            var deletedUser = GetById(id);
            if (deletedUser != null)
            {
                _repository.Delete(id);
            }
            return deletedUser;
        }

        public User Register(UserData userData)
        {
            var newUser = new User()
            {
                UserName = userData.UserName,
                PasswordHash = BC.HashPassword(userData.Password),
                DisplayName = userData.DisplayName,
                Email = userData.Email,
                ImageUrl = userData.ImageUrl
            };

            if (newUser.DisplayName == null)
            {
                newUser.DisplayName = newUser.UserName;
            }

            return _repository.Add(newUser);
        }

        public User? GetByEmail(string email)
        {
            return _repository.GetByEmail(email);
        }

        public User? GetByUsername(string userName)
        {
            return _repository.GetByUsername(userName);
        }

        public string GenerateToken(User user)
        {
            var issuer = _configuration["Jwt:Issuer"];
            var audience = _configuration["Jwt:Audience"];
            byte[] key = Encoding.ASCII.GetBytes(_configuration["Jwt:Key"]);
            SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim("Id", user.Id.ToString(CultureInfo.InvariantCulture)),
                    new Claim(JwtRegisteredClaimNames.UniqueName, user.UserName),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(1),
                Issuer = issuer,
                Audience = audience,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key),
                    SecurityAlgorithms.HmacSha512Signature)
            };
            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();
            SecurityToken token = tokenHandler.CreateToken(tokenDescriptor);
            string tokenString = tokenHandler.WriteToken(token);
            return tokenString;
        }

        public string? Login(LoginCredentials loginCredentials)
        {
            var user = GetByUsername(loginCredentials.UserName);
            if (user == null)
            {
                return null;
            }
            if (!BC.Verify(loginCredentials.Password, user.PasswordHash))
            {
                return null;
            }
            return GenerateToken(user);
        }

        public void ChangePassword(int id, string newPassword)
        {
            var password = BC.HashPassword(newPassword);
            var user = GetById(id);
            user.PasswordHash = password;
            _repository.Update(id, user);
        }
    }
}
