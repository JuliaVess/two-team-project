﻿using TwoTeamProject.Models.Telemetry;

namespace TwoTeamProject.Services
{
    public interface ITelemetryService
    {
        public List<Telemetry> GetAllTelemetries();
        public Telemetry? Add(Telemetry telemetry);
    }
}
