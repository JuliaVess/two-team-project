﻿using TwoTeamProject.Models.User;

namespace TwoTeamProject.Services
{
    public interface IUserService
    {
        public User GetById(int id);
        public List<User> GetAll();
        public void Update(int id, UserUpdate userUpdate);
        public User? Delete(int id);
        public User Register(UserData userData);
        public User? GetByEmail(string email);
        public User? GetByUsername(string userName);
        public string GenerateToken(User user);
        public string? Login(LoginCredentials loginCredentials);
        public void ChangePassword(int id, string newPassword);
    }
}
