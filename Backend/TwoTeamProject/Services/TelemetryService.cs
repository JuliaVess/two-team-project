﻿using TwoTeamProject.Models.Telemetry;
using TwoTeamProject.Repositories;

namespace TwoTeamProject.Services
{
    public class TelemetryService : ITelemetryService
    {
        private readonly ITelemetryRepository _repository;
        public TelemetryService(ITelemetryRepository repository)
        {
            _repository = repository;
        }

        public List<Telemetry> GetAllTelemetries()
        {
            return _repository.GetAllTelemetries();
        }

        public Telemetry? Add(Telemetry telemetry)
        {
            var newTelemetry = new Telemetry()
            {
                Temperature = telemetry.Temperature,
                Humidity = telemetry.Humidity,
                AirPressure = telemetry.AirPressure,
                BatteryPower = telemetry.BatteryPower,
                MovementCounter = telemetry.MovementCounter,
                MeasureSequence = telemetry.MeasureSequence,
                Mac = telemetry.Mac,
                DateTime = DateTime.Now
            };
            return _repository.Add(newTelemetry);
        }
    }
}
