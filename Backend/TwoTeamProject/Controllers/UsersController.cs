﻿using Microsoft.AspNetCore.Authorization;
using System.Globalization;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.JsonPatch.Exceptions;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using TwoTeamProject.Services;
using TwoTeamProject.Models.User;

namespace TwoTeamProject.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult GetAll()
        {
            return Ok(_userService.GetAll().Select(user => new UserView(user)));
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var selectedUser = _userService.GetById(id);
            if (selectedUser == null)
            {
                return NotFound();
            }
            return Ok(new UserView(selectedUser));
        }

        [AllowAnonymous]
        [HttpDelete]
        public IActionResult Delete()
        {
            var id = Int32.Parse(User.Claims.FirstOrDefault(claim => claim.Type == "Id").Value, CultureInfo.InvariantCulture);
            var deletedUser = _userService.Delete(id);
            if (deletedUser == null)
            {
                return NotFound();
            }
            return Ok(new UserView(deletedUser));
        }

        [AllowAnonymous]
        [HttpPut]
        public IActionResult Update([FromBody] UserUpdate userUpdate)
        {
            var id = Int32.Parse(User.Claims.FirstOrDefault(claim => claim.Type == "Id").Value, CultureInfo.InvariantCulture);
            if (_userService.GetById(id) == null)
            {
                return NotFound();
            }

            if (!TryValidateModel(userUpdate))
            {
                return BadRequest(ModelState);
            }

            _userService.Update(id, userUpdate);
            return NoContent();
        }

        [AllowAnonymous]
        [HttpPatch]
        public IActionResult PartiallyUpdateUser([FromBody] JsonPatchDocument<UserUpdate> patchDocument)
        {
            var id = Int32.Parse(User.Claims.FirstOrDefault(claim => claim.Type == "Id").Value, CultureInfo.InvariantCulture);
            var selectedUser = _userService.GetById(id);
            if (selectedUser == null)
            {
                return NotFound();
            }

            var selectedUserUpdate = new UserUpdate()
            {
                DisplayName = selectedUser.DisplayName,
                Email = selectedUser.Email,
                ImageUrl = selectedUser.ImageUrl
            };

            try
            {
                patchDocument.ApplyTo(selectedUserUpdate);
            }
            catch (JsonPatchException e)
            {
                ModelState.AddModelError("patchDocument", e.Message);
                return BadRequest(ModelState);
            }
            catch (ArgumentNullException e)
            {
                ModelState.AddModelError("patchDocument", e.Message);
                return BadRequest(ModelState);
            }

            if (!TryValidateModel(selectedUserUpdate))
            {
                return BadRequest(ModelState);
            }

            _userService.Update(id, selectedUserUpdate);
            return NoContent();
        }

        [AllowAnonymous]
        [HttpPost("Register")]
        public IActionResult Register([FromBody] UserData userData)
        {
            if (_userService.GetByUsername(userData.UserName) != null)
            {
                return Conflict("Tämä käyttäjänimi on jo käytössä");
            }

            if (userData.Email != null && _userService.GetByEmail(userData.Email) != null)
            {
                return Conflict("Tämä sähköpostiosoite on jo käytössä");
            }

            if (!TryValidateModel(userData))
            {
                return BadRequest(ModelState);
            }

            var newUser = _userService.Register(userData);
            return Created(Request.GetEncodedUrl() + "/" + newUser.Id, new UserView(newUser));
        }

        [AllowAnonymous]
        [HttpPost("Login")]
        public IActionResult Login([FromBody] LoginCredentials loginCredentials)
        {
            var user = _userService.GetByUsername(loginCredentials.UserName);
            var token = _userService.Login(loginCredentials);
            if (token == null)
            {
                return Unauthorized();
            }
            return Ok(new { tokenString = token, userId = user.Id });
        }

        [AllowAnonymous]
        [HttpPost("ChangePassword")]
        public IActionResult ChangePassword([FromBody] PasswordChangeData passwordData)
        {
            var user = _userService.GetById(Int32.Parse(User.Claims.FirstOrDefault(claim => claim.Type == "Id").Value, CultureInfo.InvariantCulture));
            if (!BC.Verify(passwordData.OldPassword, user.PasswordHash))
            {
                return Unauthorized();
            }
            _userService.ChangePassword(user.Id, passwordData.NewPassword);
            return NoContent();
        }
    }
}
