﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TwoTeamProject.Models.Telemetry;
using TwoTeamProject.Services;

namespace TwoTeamProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TelemetryController : ControllerBase
    {
        private readonly ITelemetryService _telemetryService;

        public TelemetryController(ITelemetryService telemetryService)
        {
            _telemetryService = telemetryService;
        }

        [AllowAnonymous]
        [HttpGet("GetTelemetrys")]
        public IActionResult GetAllTelemetries()
        {
            return Ok(_telemetryService.GetAllTelemetries());
        }

        [AllowAnonymous]
        [HttpPost("AddNewTelemetry")]
        public IActionResult Add([FromBody] Telemetry telemetry)
        {

            if (!TryValidateModel(telemetry))
            {
                return BadRequest();
            }
            _telemetryService.Add(telemetry);

            return Created(Request.Path, telemetry);
        }
    }
}
